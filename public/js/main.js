const input = document.getElementById('search-input');
const content = document.getElementById('content');
const hiddenMsg = document.getElementById('hiddenMsg');

const cssRules = Array.from(Array.from(document.styleSheets).find(s => s.href.endsWith('/css/main.css')).cssRules);
const imgStyle = cssRules.find(r => r.selectorText === 'img').style;

const params = new URLSearchParams(window.location.search);

const page = parseInt(params.get('page')) || 1;

const query = params.get('search') || params.get('pool');
if (query) {
	input.value = query;
	toggleContent();
}

if (page <= 1) {
	Array.from(document.getElementsByClassName('back-button')).forEach(b => b.disabled = true);
} else if (!query) {
	toggleContent();
}

document.addEventListener('keypress', e => {
	if (e.target.tagName.toLowerCase() === 'input') return;

	if (e.key === 'ArrowLeft') {
		pageBack();
	} else if (e.key === 'ArrowRight') {
		pageForward();
	}
});

function search() {
	let mode = 'search';
	params.delete('pool');

	if (/^\d+$/.test(input.value)) {
		alert('pool searches are temporarily disabled while e621 updates their site');
		return;
		mode = 'pool';
		params.delete('search');
	}

	if (input.value) {
		params.set(mode, input.value);
	} else {
		params.delete(mode);
	}
	if (input.value !== (query || '')) params.delete('page');

	window.location = `http://${window.location.host}/?${params}`;
}

function toggleContent() {
	if (content.style.display === 'none') {
		content.style.display = 'flex';
		hiddenMsg.style.display = 'none';
	} else {
		content.style.display = 'none';
		hiddenMsg.style.display = 'flex';
	}
}

function toggleComicMode() {
	if (content.style['flex-direction'] === '') {
		imgStyle['max-height'] = '';
		content.style['flex-direction'] = 'column';
	} else {
		imgStyle['max-height'] = '100vh';
		content.style['flex-direction'] = '';
	}
}

function pageBack() {
	if (page <= 1) return;
	if (!params.has('page')) params.set('page', 1);
	params.set('page', parseInt(params.get('page')) - 1);
	search();
}
function pageForward() {
	if (!params.has('page')) params.set('page', 1);
	params.set('page', parseInt(params.get('page')) + 1);
	search();
}
