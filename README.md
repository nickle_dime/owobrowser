# owobrowser

An e621 browser designed around fluid UX.

## Installation

Currently, owobrowser only requires Kemal (and its spec helper) as a dependency. Install it with `shards install`.

### Development Environment Setup

1) Begin by installing [crystal](https://crystal-lang.org/docs/installation/) along with `libssl-dev`, `libxml2-dev`, and `libyaml-dev`.
2) Clone the repository.
3) Run `shards install` in the root of the repository to install dependencies.
4) The server can now be ran as described below in **Usage**.

## Usage

Run `crystal src/owobrowser.cr` to start the server. By default, it will listen on port 3000, although this can be changed with the `-p`/`--port` switch.

## Development

TODO: Write development instructions here

Note: you will need a linux system for development.
