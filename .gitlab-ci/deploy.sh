#!/bin/bash

# Dependencies, I guess
apt-get update -y
which ssh-agent || apt-get install -y openssh-client
which git || apt-get install -y git

# Set up SSH stuff
eval "$(ssh-agent -s)"
chmod 0600 $SSH_PRIVATE_KEY
ssh-add $SSH_PRIVATE_KEY > /dev/null
mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -p $PORT $IP > ~/.ssh/known_hosts
chmod 644 ~/.ssh/known_hosts

# Get commits, extra build commands
ssh apps@$IP -p $PORT <<EOF
  cd $DEPLOY_DIR
  git pull
  shards install
  crystal build --release src/owobrowser.cr -o bin/owobrowser
  systemctl --user restart $SERVICE.service
EOF
