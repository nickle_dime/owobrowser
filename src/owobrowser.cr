require "http/client"
require "json"
require "kemal"

require "./models/*"

HEADERS = HTTP::Headers{"User-Agent" => "owobrowser/1.0 (by nickle_dime)"}

get "/faq" do |env|
  send_file env, "src/views/faq.html"
end

get "/update" do |env|
  send_file env, "src/views/update.html"
end

get "/" do |env|
  page = env.params.query["page"]? || 1
  url =
  # no pools temporarily while we figure out the new API
  #  if env.params.query["pool"]?
  #    "https://e621.net/pool/show.json?page=#{page}&id=#{env.params.query["pool"]}"
  #  else
      "https://e621.net/posts.json?page=#{page}&tags=#{URI.encode(env.params.query["search"]? || "")}"
  #  end

  res = HTTP::Client.get(url, HEADERS)
  if !res.success?
    env.response.status_code = res.status_code
    raise Kemal::Exceptions::CustomException.new(env)
  end
  body = res.body

  posts = [] of Post

  error_msg = ""

  begin
    err = E621Error.from_json(body)
    error_msg = err.reason unless err.success
  rescue
    posts = PostResponse.from_json(body).posts.reject do |p|
      p.file.ext == "swf" || p.file.ext == "webm" || p.file.url.nil?
    end
  end

  render "src/views/main.ecr"
end

error 500 do |env|
  error_msg = "internal error"
  render "src/views/error.ecr"
end

error 503 do |env|
  error_msg = "e621 error"
  render "src/views/error.ecr"
end

Kemal.run
