require "json"

class TmpJSONTime
  include JSON::Serializable

  getter json_class : String

  @[JSON::Field(key: "s")]
  getter secs : Int64

  @[JSON::Field(key: "n")]
  getter nsecs : Int32
end

class JSONTimeConverter
  def self.from_json(value : JSON::PullParser) : Time
    tmp = TmpJSONTime.new(value)

    raise "Invalid json_class (must be Time)" unless tmp.json_class == "Time"

    Time.utc(seconds: tmp.secs, nanoseconds: tmp.nsecs)
  end

  def self.to_json(value : Time, json : JSON::Builder)
    json.object do
      json.field("json_class", "Time")
      json.field("s", value.epoch)
      json.field("n", value.nanosecond)
    end
  end
end
