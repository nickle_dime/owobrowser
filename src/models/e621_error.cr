require "json"

class E621Error
  include JSON::Serializable
  include JSON::Serializable::Strict

  property success : Bool
  property reason : String = ""
end
