require "json"
require "./converters"

struct PostResponse
  include JSON::Serializable

  property posts : Array(Post)
end

class Post
  include JSON::Serializable

  # @[JSON::Field(converter: JSONTimeConverter)]
  # property created_at : Time

  property id : Int32
  # property tags : String # TODO: maybe custom tag matcher class?
  # property description : String
  # property creator_id : Int32?
  # property author : String
  # property score : Int32
  # property fav_count : Int32
  # property rating : String
  # property artist : Array(String)
  # property sources = Array(String).new

  property file : PostFile
end

record PostFile, ext : String, url : String? do
  include JSON::Serializable
end
