require "json"
require "./post"

class Pool
  include JSON::Serializable

  @[JSON::Field(converter: JSONTimeConverter)]
  property created_at : Time
  @[JSON::Field(converter: JSONTimeConverter)]
  property updated_at : Time

  property id : Int32
  property name : String
  property post_count : Int32
  property posts : Array(Post)
end
